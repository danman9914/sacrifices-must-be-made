﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cub : SpecialCardBehaviour
{
    public override IEnumerator OnSlotTargetedForAttack()
    {
        if (Card.Info.HasTrait(Trait.Juvenile))
        {
            Trait typeTrait = Card.Info.HasTrait(Trait.Bear) ? Trait.Bear : Trait.Wolf;
            var protector = BoardManager.instance.CardsOnBoard.Find(x => x.Info.HasTrait(Trait.ProtectsCub) && x.Info.HasTrait(typeTrait));
            if (protector != null)
            {
                yield return StartCoroutine(BoardManager.instance.SwitchSlotsOfCards(Card, protector, 3f));
            }
        }
        yield break;
    }
}