﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warren : SpecialCardBehaviour
{

    public override IEnumerator OnSacrifice()
    {
        if (BoardManager.instance.CurrentCardToPlay.Info.HasTrait(Trait.EatsWarrens))
        {
            Card.Anim.SetMarkedForSacrifice(false, false);

            for (int i = 0; i < 3; i++)
            {
                Card.Displayer.SetPortrait(Resources.Load<Texture>("Art/Cards/Portraits/portrait_warren_eaten" + (i + 1).ToString()));
                GetComponent<Animator>().SetTrigger("sacrifice_selected");
                //todo eat sound

                yield return new WaitForSeconds(0.4f);
            }
        }
    }
}
