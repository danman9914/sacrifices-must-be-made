﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat : SpecialCardBehaviour
{
    int sacrificeCount;

    public override IEnumerator OnSacrifice()
    {
        sacrificeCount++;

        if (sacrificeCount >= 9)
        {
            yield return new WaitForSeconds(0.25f);

            var undeadCat = Resources.Load<CardInfo>("Data/Cards/CatUndead");
            yield return StartCoroutine(Card.TransformIntoCard(undeadCat));
        }
    }
}
