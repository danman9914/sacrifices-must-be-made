﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarrenEater : SpecialCardBehaviour
{
    public override IEnumerator OnPlayFromHand()
    {
        if (BoardManager.instance.LastSacrificesInfo.Find(x => x.HasTrait(Trait.FeedsStoat)))
        {
            Card.Displayer.SetName("Bloated " + Card.Info.displayedName);
            Card.SwitchToAlternatePortrait();
            Card.AddHealth(1);
        }
        yield break;
    }
}