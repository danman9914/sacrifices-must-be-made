﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beehive : SpecialCardBehaviour
{
    public override IEnumerator OnResolveOnBoard()
    {
        yield return new WaitForSeconds(0.15f);

        foreach (CardSlot slot in BoardManager.instance.PlayerSlots)
        {
            CheckForAttraction(slot);
        }
    }

    public override IEnumerator OnOtherCardResolve(Card otherCard)
    {
        yield return new WaitForSeconds(0.15f);

        CheckForAttraction(otherCard.Slot);
    }

    private void CheckForAttraction(CardSlot slot)
    {
        if (slot != null && slot.Card != null && slot.Card.Info.HasTrait(Trait.LikesHoney))
        {
            if (BoardManager.instance.SlotIsLeftOfSlot(slot, Card.Slot, BoardManager.instance.PlayerSlots))
            {
                var leftAdjacent = BoardManager.instance.GetAdjacent(Card.Slot, true);
                if (leftAdjacent.Card == null)
                {
                    AttractHoneyLover(slot.Card, leftAdjacent);
                }
            }
            else
            {
                var rightAdjacent = BoardManager.instance.GetAdjacent(Card.Slot, false);
                if (rightAdjacent.Card == null)
                {
                    AttractHoneyLover(slot.Card, rightAdjacent);
                }
            }
        }
    }

    private void AttractHoneyLover(Card honeyLover, CardSlot slot)
    {
        BoardManager.instance.AssignCardToSlot(honeyLover, slot);
        honeyLover.SwitchToAlternatePortrait();
    }
}
