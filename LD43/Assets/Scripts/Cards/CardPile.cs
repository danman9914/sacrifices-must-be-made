﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class CardPile : MonoBehaviour
{
    public Vector3 TopCardPosition
    {
        get
        {
            return transform.position + (Vector3.up * cards.Count * Y_SPACING);
        }
    }

    [SerializeField]
    private GameObject cardbackPrefab;

    [SerializeField]
    private GameObject shadow;

    [SerializeField]
    private int fixedPileSize;

    private List<Transform> cards = new List<Transform>();

    private const float ROTATION_VARIANCE = 15f;
    private const float Y_SPACING = 0.01f;

    private void Start()
    {
        if (fixedPileSize > 0)
        {
            SpawnCards(fixedPileSize);
        }
    }

    public void SpawnCards(int numCards)
    {
        for (int i = 0; i < numCards; i++)
        {
            var cardObj = Instantiate(cardbackPrefab);
            cardObj.transform.SetParent(transform);
            cardObj.transform.localRotation = Quaternion.Euler(90f, 0f, -(ROTATION_VARIANCE/2f) + (Random.value * ROTATION_VARIANCE));
            cardObj.transform.localPosition = new Vector3(0f, i * Y_SPACING, 0f);
            cards.Add(cardObj.transform);
        }
    }

    public void Draw()
    {
        if (cards.Count > 0)
        {
            Transform card = cards[cards.Count - 1];
            Tween.Position(card, card.transform.position + (Vector3.up * 1f) + (Vector3.back * 5f), 0.5f, 0f, Tween.EaseInOut);
            Tween.Rotation(card, Quaternion.Euler(-180f, 180f, 0f), 0.5f, 0f, Tween.EaseInOut);
            cards.Remove(card);
            Destroy(card.gameObject, 2f);

            if (cards.Count == 0)
            {
                shadow.SetActive(false);
            }
        }
    }
}
