﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : Interactable
{
    public bool AttackedThisTurn { get; set; }

    public int NumTurnsInPlay { get; set; }

    public int Attack { get { return attack; } }
    private int attack;
    private int health;

    private int attackBuff;

    public CardInfo Info { get { return info; } }
    private CardInfo info;

    public CardDisplayer Displayer { get { return GetComponentInChildren<CardDisplayer>(); } }
    public CardAnimationController Anim { get { return GetComponent<CardAnimationController>(); } }
    public CardSlot Slot { get; set; }
    public CardSlot QueuedSlot { get; set; }

    private SpecialCardBehaviour specialBehaviour;

    private bool movingLeft = true;

    public void UpdateHandPosition(Vector3 handPos)
    {
        transform.localPosition = Vector3.Lerp(transform.localPosition, new Vector3(handPos.x, handPos.y, transform.localPosition.z), Time.deltaTime * 8f);
    }

    public void SetInfo(CardInfo info)
    {
        this.info = info;

        attack = info.baseAttack;
        health = info.baseHealth;

        Displayer.DisplayInfo(info);

        if (!string.IsNullOrEmpty(info.specialScriptName))
        {
            var type = System.Type.GetType(info.specialScriptName);
            specialBehaviour = gameObject.AddComponent(type) as SpecialCardBehaviour;
        }
        else
        {
            specialBehaviour = null;
        }
    }

    public void SwitchToAlternatePortrait()
    {
        Displayer.SetPortrait(info.alternatePortraitTex);
    }

    public void SetIsOpponentCard()
    {
        Anim.FlipCardRenderer();
    }

    public bool CanAttackDirectly()
    {
        return Slot.opposingSlot.Card == null || Info.ability == SpecialAbility.Flying;
    }

    public void AddHealth(int amount)
    {
        health += amount;
        UpdateStatsText();
    }

    public IEnumerator Play()
    {
        if (specialBehaviour != null)
        {
            yield return StartCoroutine(specialBehaviour.OnPlayFromHand());
        }
    }

    public IEnumerator ResolveOnBoard()
    {
        if (specialBehaviour != null)
        {
            yield return StartCoroutine(specialBehaviour.OnResolveOnBoard());
        }

        if (Info.ability == SpecialAbility.DrawRabbits)
        {
            ViewManager.instance.SwitchToView(View.Hand);
            yield return new WaitForSeconds(0.5f);
            yield return StartCoroutine(PlayerHand.instance.Draw(Resources.Load<CardInfo>("Data/Cards/Rabbit")));
            yield return new WaitForSeconds(0.2f);
            yield return StartCoroutine(PlayerHand.instance.Draw(Resources.Load<CardInfo>("Data/Cards/Rabbit")));
        }
    }

    public IEnumerator OtherCardResolve(Card otherCard)
    {
        if (specialBehaviour != null)
        {
            yield return StartCoroutine(specialBehaviour.OnOtherCardResolve(otherCard));
        }
    }

    public IEnumerator OnSlotTargetedForAttack(Card otherCard)
    {
        if (specialBehaviour != null)
        {
            yield return StartCoroutine(specialBehaviour.OnSlotTargetedForAttack());
        }
    }

    public IEnumerator TakeDamage(int damage, Card attacker)
    {
        AudioController.Instance.PlaySound("die");
        health = Mathf.Max(health - damage, 0);
        
        if (health <= 0 || (attacker != null && attacker.Info.ability == SpecialAbility.Deathtouch))
        {
            Die();
        }

        UpdateStatsText();
        Anim.PlayHitAnimation();

        if (info.ability == SpecialAbility.BeesOnHit)
        {
            yield return new WaitForSeconds(0.25f);
            ViewManager.instance.SwitchToView(View.Hand);
            yield return new WaitForSeconds(0.3f);
            yield return StartCoroutine(PlayerHand.instance.Draw(Resources.Load<CardInfo>("Data/Cards/Bee")));
            yield return new WaitForSeconds(0.6f);
            ViewManager.instance.SwitchToView(View.Board);
            yield return new WaitForSeconds(0.3f);
        }

        yield break;
    }

    public IEnumerator DoPostAttackActions()
    {
        if (Info.ability == SpecialAbility.MoveAfterAttack)
        {
            var toLeft = BoardManager.instance.GetAdjacent(Slot, true);
            var toRight = BoardManager.instance.GetAdjacent(Slot, false);
            bool toLeftValid = toLeft != null && toLeft.Card == null;
            bool toRightValid = toRight != null && toRight.Card == null;

            if (movingLeft && !toLeftValid)
            {
                movingLeft = false;
            }
            if (!movingLeft && !toRightValid)
            {
                movingLeft = true;
            }

            CardSlot destination = movingLeft ? toLeft : toRight;
            bool destinationValid = movingLeft ? toLeftValid : toRightValid;

            Displayer.SetIconFlipped(movingLeft);

            if (destination != null && destinationValid)
            {
                ViewManager.instance.SwitchToView(View.Board);

                yield return new WaitForSeconds(0.25f);
                BoardManager.instance.AssignCardToSlot(this, destination);
                yield return new WaitForSeconds(0.25f);
            }
        }

        yield break;
    }

    public IEnumerator Sacrifice()
    {
        if (info.ability == SpecialAbility.Sacrificial)
        {
            // TODO cat sound
            Anim.SetMarkedForSacrifice(false);
            Anim.PlaySacrificeParticles();

            if (specialBehaviour != null)
            {
                yield return StartCoroutine(specialBehaviour.OnSacrifice());
            }
        }
        else
        {
            if (specialBehaviour != null)
            {
                yield return StartCoroutine(specialBehaviour.OnSacrifice());
            }

            Die();
        }

        yield break;
    }

    public IEnumerator OnUpkeep()
    {
        NumTurnsInPlay++;
        if (info.ability == SpecialAbility.Evolve)
        {
            int turnsRemaining = Mathf.Max(1, info.abilityIntParameter - NumTurnsInPlay);
            Displayer.SetAbilityIcon(Resources.Load<Texture>("Art/Cards/AbilityIcons/ability_evolve_" + turnsRemaining));
            if (specialBehaviour != null)
            {
                if (NumTurnsInPlay >= info.abilityIntParameter)
                {
                    yield return StartCoroutine(TransformIntoCard(info.abilityCardParameter));

                    yield return StartCoroutine(SceneSequencer.instance.OnCardTransform(this));
                }
            }
        }
        if (specialBehaviour != null)
        {
            yield return StartCoroutine(specialBehaviour.OnUpkeep());
        }
    }

    public IEnumerator TransformIntoCard(CardInfo evolvedInfo)
    {
        ViewManager.instance.SwitchToView(View.Board);
        yield return new WaitForSeconds(0.15f);

        Anim.PlayTransformAnimation();
        yield return new WaitForSeconds(0.15f);
        SetInfo(evolvedInfo);

        yield return new WaitForSeconds(0.5f);
    }

    public void Die(bool playSound = true)
    {
        if (Slot != null)
        {
            Slot.Card = null;
        }

        Anim.PlayDeathAnimation(playSound);
        Destroy(gameObject, 2f);
    }

    private void UpdateStatsText()
    {
        Displayer.SetStatsText(Attack, health);
    }

    #region Interaction
    public override void OnCursorSelectStart()
    {
        if (PlayerHand.instance != null)
        {
            PlayerHand.instance.OnCardSelected(this);
        }
    }

    public override void OnCursorEnter()
    {
        if (PlayerHand.instance != null)
        {
            PlayerHand.instance.OnCardInspected(this);
        }
    }
    #endregion
}
