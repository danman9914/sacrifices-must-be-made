﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deck : MonoBehaviour
{

    public int CardsInDeck { get { return cards.Count; } }

    [SerializeField]
    private List<CardInfo> fixedDraws = new List<CardInfo>();

    [SerializeField]
    private List<CardInfo> cards = new List<CardInfo>();

    [SerializeField]
    private bool usePlayerDeck;

    [SerializeField]
    private GameObject cardPrefab;

    void Awake()
    {
        if (usePlayerDeck)
        {
            cards = new List<CardInfo>();
            cards.AddRange(GameStats.playerDeck);
        }
    }

    public Card DrawSpecificCard(CardInfo info)
    {
        var cardGO = SpawnCard();
        var card = cardGO.GetComponent<Card>();

        card.SetInfo(info);

        return card;
    }

	public Card DrawCard()
    {
        if (cards.Count == 0)
        {
            return null;
        }

        CardInfo info = null;
        while (info == null && fixedDraws.Count > 0)
        {
            if (cards.Contains(fixedDraws[0]))
            {
                info = fixedDraws[0];
            }
            fixedDraws.RemoveAt(0);
        }
        
        if (info == null)
        {
            info = cards[Random.Range(0, cards.Count)];
        }
        cards.Remove(info);

        var cardGO = SpawnCard();
        var card = cardGO.GetComponent<Card>();

        card.SetInfo(info);

        return card;
    }

    private GameObject SpawnCard()
    {
        var card = Instantiate(cardPrefab);
        return card;
    }
}
