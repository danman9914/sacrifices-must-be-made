﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

//TODO move this somewhere else?
public enum BodySacrifice
{
    None,
    RightEye,
    Hand,
}

public class FirstPersonKnife : MonoBehaviour
{
    public static FirstPersonKnife instance;

    [SerializeField]
    private Animator anim;

    private bool madeSelection;
    private BodySacrifice selection;
    private BodySacrifice specificSelectionAllowed;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        SetActivated(false);
    }

    public IEnumerator ShowKnifeAndWaitForSelection(System.Action<BodySacrifice> callback, BodySacrifice specificAllowed = BodySacrifice.None)
    {
        SetActivated(true);

        ViewManager.instance.SwitchToView(View.Default);

        madeSelection = false;
        specificSelectionAllowed = specificAllowed;

        yield return new WaitForEndOfFrame();
        yield return new WaitUntil(() => madeSelection);

        if (callback != null)
        {
            callback(selection);
        }

        SetActivated(false);
    }

    private void SetActivated(bool activated)
    {
        anim.gameObject.SetActive(activated);
        enabled = activated;
    }

    private void Update()
    {
        BodySacrifice currentSacrifice = GetSacrificeForCursorPosition();

        Vector3 intendedRotation = Vector3.zero;
        Vector3 intendedPosition = Vector3.zero;

        Cursor3D.instance.SetCursorType(CursorType.Place);
        switch (GetSacrificeForCursorPosition())
        {
            case BodySacrifice.Hand:
                intendedPosition = new Vector3(-1.5f, -1f, 1.5f);
                intendedRotation = new Vector3(0f, 0f, 20f);
                Cursor3D.instance.SetCursorType(CursorType.Sacrifice);
                break;
            case BodySacrifice.RightEye:
                intendedPosition = new Vector3(-1.3f, 0f, 1.4f);
                intendedRotation = new Vector3(30f, 38f, 3f);
                Cursor3D.instance.SetCursorType(CursorType.Sacrifice);
                break;
            case BodySacrifice.None:
                intendedPosition = new Vector3(-2.4f, 0f, 2f);
                intendedRotation = new Vector3(0f, 0f, 50f);
                break;

        }
        transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(intendedRotation), Time.deltaTime * 12f);
        transform.localPosition = Vector3.Lerp(transform.localPosition, intendedPosition, Time.deltaTime * 12f);

        if (Input.GetButtonDown("Select"))
        {
            selection = currentSacrifice;
            madeSelection = true;
            Cursor3D.instance.SetCursorType(CursorType.Default);
        }
    }

    private BodySacrifice GetSacrificeForCursorPosition()
    {
        Vector2 cursorPos = Cursor3D.instance.NormalizedPosition;
        if ((cursorPos.x > 0f && cursorPos.y > 0f && specificSelectionAllowed == BodySacrifice.None) ||
            (specificSelectionAllowed == BodySacrifice.RightEye && cursorPos.x > 0f))
        {
            return BodySacrifice.RightEye;
        }
        else if (cursorPos.y < 0f && (specificSelectionAllowed == BodySacrifice.None || specificSelectionAllowed == BodySacrifice.Hand))
        {
            return BodySacrifice.Hand;
        }

        return BodySacrifice.None;
    }
}
