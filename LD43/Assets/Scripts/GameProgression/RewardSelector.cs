﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class RewardSelector : MonoBehaviour
{
    public static RewardSelector instance;

    [SerializeField]
    private List<RewardCard> rewards;

    [SerializeField]
    private CardPile pile;

    private RewardCard chosenReward;

    private void Awake()
    {
        instance = this;
    }

    public IEnumerator ChooseReward(List<CardInfo> options)
    {
        ViewManager.instance.LockState = ViewLockState.Locked;
        ViewManager.instance.SwitchToView(View.Choices);

        SetCollidersEnabled(false);

        foreach (CardInfo option in options)
        {
            var card = rewards[options.IndexOf(option)];

            card.gameObject.SetActive(true);
            card.GetComponent<CardAnimationController>().SetFlipped(true, true);
            card.SetInfo(option);

            AudioController.Instance.PlaySoundWithPitch("card", 0.9f + (Random.value * 0.2f), 0.2f);

            Vector3 cardPos = card.transform.position;
            card.transform.position = card.transform.position + (Vector3.forward * 5f) + new Vector3(-0.5f + (Random.value * 1f), 0f, 0f);
            Tween.Position(card.transform, cardPos, 0.3f, 0f, Tween.EaseInOut);
            Tween.Rotate(card.transform, new Vector3(0f, 0f, Random.value * 1.5f), Space.Self, 0.4f, 0f, Tween.EaseOut);
            yield return new WaitForSeconds(0.2f);

            var emission = card.GetComponentInChildren<ParticleSystem>().emission;
            emission.rateOverTime = 0f;
        }

        yield return new WaitForSeconds(0.4f);
        SetCollidersEnabled(true);

        chosenReward = null;
        yield return new WaitUntil(() => chosenReward != null);
        yield return new WaitForSeconds(0.5f);

        foreach (RewardCard card in rewards)
        {
            if (card != chosenReward)
            {
                Tween.Position(card.transform, card.transform.position + (Vector3.forward * 20f), 0.5f, 0f, Tween.EaseInOut);
                Destroy(card.gameObject, 0.5f);
                yield return new WaitForSeconds(0.1f);
            }
        }

        GameStats.playerDeck.Add(chosenReward.Info);

        ViewManager.instance.LockState = ViewLockState.Unlocked;
    }

    public void OnRewardChosen(RewardCard card)
    {
        chosenReward = card;
        SetCollidersEnabled(false);

        card.GetComponent<CardAnimationController>().SetFlipped(true);
        Tween.Position(card.transform, pile.transform.position + (Vector3.up * 1f), 0.5f, 0f, Tween.EaseInOut);
        Tween.Position(card.transform, pile.TopCardPosition, 0.25f, 0.75f, Tween.EaseInOut);
    }

    private void SetCollidersEnabled(bool collidersEnabled)
    {
        foreach (RewardCard card in rewards)
        {
            card.GetComponent<Collider>().enabled = collidersEnabled;
        }
    }
}
