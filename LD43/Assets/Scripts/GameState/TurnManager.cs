﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TurnManager : MonoBehaviour
{
    public static TurnManager instance;

    public OpponentAI opponent { get; set; }

    public bool IsPlayerTurn { get; set; }
    public bool IsCombatPhase { get; set; }

    void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        StartCoroutine(GameSequence());
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    private IEnumerator GameSequence()
    {
        if (GameStats.sceneProgress > 0)
        {
            AudioController.Instance.SetLoop("main_loop");
            AudioController.Instance.SetLoopVolume(0f);
            AudioController.Instance.FadeInLoop(0.2f, 0.75f);
        }

        yield return new WaitForEndOfFrame();
        if (SceneSequencer.instance == null)
        {
            yield break;
        }
        PlayerHand.instance.PlayingLocked = true;

        yield return StartCoroutine(SceneSequencer.instance.Intro());

        yield return new WaitForSeconds(0.5f);

        int startingHandSize = 3;
        for (int i = 0; i < startingHandSize; i++)
        {
            yield return StartCoroutine(PlayerHand.instance.Draw());
        }

        if (opponent.QueueFirstCardBeforePlayer)
        {
            opponent.QueueNewCards();
        }

        while (!GameHasEnded())
        {
            yield return StartCoroutine(PlayerTurn());

            if (GameHasEnded())
            {
                break;
            }

            yield return StartCoroutine(OpponentTurn());
        }

        AudioController.Instance.FadeOutLoop(0.5f);
        yield return StartCoroutine(SceneSequencer.instance.End(PlayerIsWinner()));
    }

    private IEnumerator PlayerTurn()
    {
        IsPlayerTurn = true;
        ViewManager.instance.LockState = ViewLockState.Locked;

        yield return StartCoroutine(DoUpkeepPhase(true));
        yield return StartCoroutine(SceneSequencer.instance.PlayerTurnStart());

        ViewManager.instance.SwitchToView(View.Default);

        StartCoroutine(SceneSequencer.instance.PlayerDuringTurn());
        yield return new WaitForSeconds(0.25f);

        if (PlayerHand.instance != null)
        {
            yield return StartCoroutine(PlayerHand.instance.Draw());
        }
        yield return new WaitForSeconds(0.25f);

        CombatBell.instance.Enabled = true;

        if (PlayerHand.instance != null)
        {
            PlayerHand.instance.PlayingLocked = false;
        }
        ViewManager.instance.LockState = ViewLockState.Unlocked;

        while (!CombatBell.instance.Rang)
        {
            if (GameHasEnded())
            {
                yield break;
            }
            yield return new WaitForEndOfFrame();
        }
        CombatBell.instance.Rang = false;

        yield return StartCoroutine(DoCombatPhase(playerIsAttacker: true));
    }

    private IEnumerator OpponentTurn()
    {
        IsPlayerTurn = false;
        if (PlayerHand.instance != null)
        {
            PlayerHand.instance.PlayingLocked = true;
        }
        ViewManager.instance.LockState = ViewLockState.Locked;

        yield return StartCoroutine(DoUpkeepPhase(false));
        ViewManager.instance.SwitchToView(View.Default);

        yield return StartCoroutine(SceneSequencer.instance.OpponentTurnStart());

        yield return new WaitForSeconds(0.5f);

        StartCoroutine(opponent.DoPlayPhase());

        yield return new WaitForSeconds(1f);

        yield return StartCoroutine(DoCombatPhase(playerIsAttacker: false));
    }

    private IEnumerator DoUpkeepPhase(bool playerUpkeep)
    {
        var slots = playerUpkeep ? BoardManager.instance.PlayerSlots : BoardManager.instance.OpponentSlots;
        foreach (CardSlot slot in slots)
        {
            if (slot.Card != null)
            {
                yield return StartCoroutine(slot.Card.OnUpkeep());
            }
        }
    }

    private IEnumerator DoCombatPhase(bool playerIsAttacker)
    {
        CombatBell.instance.Enabled = false;
        IsCombatPhase = true;
        var attackingSlots = playerIsAttacker ? BoardManager.instance.PlayerSlots : BoardManager.instance.OpponentSlots;
        attackingSlots.RemoveAll(x => x.Card == null || x.Card.Attack == 0);

        if (attackingSlots.Count > 0)
        {
            ViewManager.instance.SwitchToView(View.Board);
            ViewManager.instance.LockState = ViewLockState.Locked;
        }

        if (playerIsAttacker)
        {
            yield return StartCoroutine(SceneSequencer.instance.PlayerCombatStart());
        }
        else
        {
            yield return StartCoroutine(SceneSequencer.instance.OpponentCombatStart());
        }

        foreach (CardSlot slot in attackingSlots)
        {
            slot.Card.AttackedThisTurn = false;
        }

        int pooledAttackDamage = 0;
        foreach (CardSlot slot in attackingSlots)
        {
            ViewManager.instance.SwitchToView(View.Board);
            ViewManager.instance.LockState = ViewLockState.Locked;

            if (!slot.Card.AttackedThisTurn)
            {
                slot.Card.AttackedThisTurn = true;
                yield return new WaitForSeconds(0.2f);

                if (slot.Card.CanAttackDirectly())
                {
                    slot.Card.Anim.PlayAttackAnimation(true);

                    int nextSlotIndex = attackingSlots.IndexOf(slot) + 1;
                    bool nextSlotAlsoAttackingFace = nextSlotIndex < attackingSlots.Count && 
                        attackingSlots[nextSlotIndex].Card.CanAttackDirectly();

                    pooledAttackDamage += slot.Card.Attack;
                    if (nextSlotAlsoAttackingFace)
                    {
                        yield return new WaitForSeconds(0.05f);
                    }
                    else
                    {
                        yield return new WaitForSeconds(0.3f);

                        AudioController.Instance.PlaySound("die");
                        yield return StartCoroutine(LifeManager.instance.ShowDamageSequence(pooledAttackDamage, !playerIsAttacker));
                        pooledAttackDamage = 0;
                    }

                    if (GameHasEnded())
                    {
                        break;
                    }
                }
                else
                {
                    slot.Card.Anim.PlayAttackAnimation(false);
                    yield return new WaitForSeconds(0.3f);
                    slot.Card.Anim.SetAnimationPaused(true);

                    yield return StartCoroutine(slot.opposingSlot.Card.OnSlotTargetedForAttack(slot.Card));
                    slot.Card.Anim.SetAnimationPaused(false);
                    yield return new WaitForSeconds(0.3f);

                    yield return StartCoroutine(slot.opposingSlot.Card.TakeDamage(slot.Card.Attack, slot.Card));
                }
            }
        }

        foreach (CardSlot slot in attackingSlots)
        {
            if (slot.Card != null)
            {
                yield return StartCoroutine(slot.Card.DoPostAttackActions());
            }
        }

        if (playerIsAttacker)
        {
            yield return StartCoroutine(SceneSequencer.instance.PlayerCombatEnd());
        }
        else
        {
            yield return StartCoroutine(SceneSequencer.instance.OpponentCombatEnd());
        }

        IsCombatPhase = false;
        ViewManager.instance.LockState = ViewLockState.Unlocked;
        yield return new WaitForSeconds(0.5f);
    }

    private bool PlayerIsWinner()
    {
        return LifeManager.instance.Balance >= 5;
    }

    private bool GameHasEnded()
    {
        return Mathf.Abs(LifeManager.instance.Balance) >= 5;
    }
}
