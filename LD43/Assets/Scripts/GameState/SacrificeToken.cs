﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SacrificeToken : MonoBehaviour
{
    [SerializeField]
    private Texture satisfiedTexture;

    [SerializeField]
    private Texture notSatisfiedTexture;

    [SerializeField]
    private Renderer coinRenderer;

    private bool isSatisfied = false;

    public void ResetTexture()
    {
        coinRenderer.material.mainTexture = notSatisfiedTexture;
    }

    public void Show()
    {
        ResetTexture();
        StopAllCoroutines();
        gameObject.SetActive(true);
        GetComponentInChildren<Animator>().SetTrigger("enter");
    }

    public void Hide()
    {
        if (gameObject.activeSelf)
        {
            GetComponentInChildren<Animator>().SetTrigger("exit");
            StartCoroutine(WaitThenDisable());
        }
    }

    private IEnumerator WaitThenDisable()
    {
        yield return new WaitForSeconds(0.25f);
        gameObject.SetActive(false);
    }

    public void SetSatisfied(bool satisfied)
    {
        if (satisfied && !this.isSatisfied)
        {
            GetComponentInChildren<Animator>().SetTrigger("flip_satisfied");
        }
        if (!satisfied && this.isSatisfied)
        {
            //flip back
        }
        isSatisfied = satisfied;
    }

    private void KF_SwitchTextureSatisfied()
    {
        coinRenderer.material.mainTexture = satisfiedTexture;
    }
}
