﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class BoardManager : MonoBehaviour
{
    public static BoardManager instance;

    public int PlayerAvailableSacrifices { get { return playerSlots.FindAll(x => x.Card != null).Count; } }
    public List<CardSlot> PlayerSlots { get { return new List<CardSlot>(playerSlots); } }
    [SerializeField]
    private List<CardSlot> playerSlots = new List<CardSlot>();

    public int OpponentAvailableSacrifices { get { return opponentSlots.FindAll(x => x.Card != null).Count; } }
    public List<CardSlot> OpponentSlots { get { return new List<CardSlot>(opponentSlots); } }
    [SerializeField]
    private List<CardSlot> opponentSlots = new List<CardSlot>();

    public List<Card> CardsOnBoard
    {
        get
        {
            var cardsOnBoard = new List<Card>();
            foreach (CardSlot s in playerSlots)
            {
                if (s.Card != null)
                {
                    cardsOnBoard.Add(s.Card);
                }
            }
            foreach (CardSlot s in opponentSlots)
            {
                if (s.Card != null)
                {
                    cardsOnBoard.Add(s.Card);
                }
            }
            return cardsOnBoard;
        }
    }

    public CardSlot LastSelectedSlot { get; set; }
    public bool ChoosingSlot { get; set; }
    private List<CardSlot> currentValidSlots;

    public bool ChoosingSacrifices { get; set; }
    public List<CardInfo> LastSacrificesInfo { get { return lastSacrificesInfo; } }
    private List<CardInfo> lastSacrificesInfo = new List<CardInfo>();
    public List<CardSlot> CurrentSacrifices { get { return currentSacrifices; } }
    private List<CardSlot> currentSacrifices = new List<CardSlot>();

    public Card CurrentCardToPlay { get; set; }

    public bool CancelledSacrifice { get; set; }

    [SerializeField]
    private SacrificeTokens tokens;

    private bool viewChangedFromBoard;

    private const float DEFAULT_TRANSITION_SPEED = 0.1f;

    private void Awake()
    {
        instance = this;
    }

    public IEnumerator ChooseSacrificesForCards(List<CardSlot> validSlots, Card card)
    {
        ViewManager.instance.LockState = ViewLockState.Unlocked;
        ViewManager.instance.SwitchToView(View.Board);
        Cursor3D.instance.SetCursorType(CursorType.Sacrifice);

        viewChangedFromBoard = false;

        currentValidSlots = validSlots;
        ChoosingSacrifices = true;
        CancelledSacrifice = false;
        CurrentCardToPlay = card;
        LastSacrificesInfo.Clear();

        yield return StartCoroutine(tokens.ShowTokens(card.Info.cost));

        while (!(currentSacrifices.Count == card.Info.cost || viewChangedFromBoard))
        {
            tokens.SetTokensFlipped(currentSacrifices.Count);
            yield return new WaitForEndOfFrame();
        }

        foreach (CardSlot s in currentSacrifices)
        {
            LastSacrificesInfo.Add(s.Card.Info);
        }

        bool cancelledDueToNoSpaceForCard = !SacrificesCreateRoomForCard();

        if (viewChangedFromBoard || cancelledDueToNoSpaceForCard)
        {
            tokens.HideTokens();

            if (cancelledDueToNoSpaceForCard)
            {
                yield return new WaitForSeconds(0.25f);
            }

            foreach (CardSlot s in currentSacrifices)
            {
                s.Card.Anim.SetMarkedForSacrifice(false);
            }
            ViewManager.instance.SwitchToView(View.Default);
            Cursor3D.instance.SetCursorType(CursorType.Default);
            CancelledSacrifice = true;
        }
        else
        {
            tokens.SetTokensFlipped(currentSacrifices.Count);
            ChoosingSacrifices = false;
            yield return new WaitForSeconds(0.2f);

            tokens.HideTokens();

            foreach (CardSlot s in currentSacrifices)
            {
                yield return StartCoroutine(s.Card.Sacrifice());
                AudioController.Instance.PlaySoundAtLimitedFrequency("sacrifice", 0.1f);
            }
        }

        ChoosingSacrifices = false;
        currentSacrifices.Clear();
    }

    public IEnumerator ChooseSlot(List<CardSlot> validSlots, bool canCancel)
    {
        ChoosingSlot = true;
        Cursor3D.instance.SetCursorType(CursorType.Place);
        ViewManager.instance.LockState = canCancel ? ViewLockState.Unlocked : ViewLockState.LockedToBoard;
        ViewManager.instance.SwitchToView(View.Board);

        viewChangedFromBoard = false;

        currentValidSlots = validSlots;
        LastSelectedSlot = null;

        foreach (CardSlot slot in validSlots)
        {
            slot.Chooseable = true;
        }

        yield return new WaitUntil(() => LastSelectedSlot != null || (canCancel && viewChangedFromBoard));

        if (canCancel && viewChangedFromBoard)
        {
            ViewManager.instance.SwitchToView(View.Default);
        }

        foreach (CardSlot slot in validSlots)
        {
            slot.Chooseable = false;
        }

        ViewManager.instance.LockState = ViewLockState.Unlocked;
        ChoosingSlot = false;
        Cursor3D.instance.SetCursorType(CursorType.Default);
    }

    public void OnViewChangedFromBoard()
    {
        viewChangedFromBoard = true;
    }

    public IEnumerator ClearBoard()
    {
        foreach (Card c in CardsOnBoard)
        {
            c.Die(playSound: false);

            yield return new WaitForSeconds(0.05f);
        }
    }

    public IEnumerator SwitchSlotsOfCards(Card card1, Card card2, float switchSpeed = 1f)
    {
        var card1Slot = card1.Slot;
        var card2Slot = card2.Slot;
        card1.Slot = null;
        card2.Slot = null;
        AssignCardToSlot(card1, card2Slot, switchSpeed);
        AssignCardToSlot(card2, card1Slot, switchSpeed);
        yield return new WaitForSeconds(DEFAULT_TRANSITION_SPEED * switchSpeed);
    }

    public void ShowCardNearBoard(Card card, bool showNearBoard)
    {
        card.GetComponent<Collider>().enabled = !showNearBoard;
        if (showNearBoard)
        {
            var cameraParent = Camera.main.transform.Find("CardNearBoardParent");
            card.transform.parent = cameraParent;

            Vector3 boardPosition = new Vector3(-2.6f, -1.7f, 3.7f);
            card.transform.localPosition = boardPosition + (Vector3.left * 3f);

            Tween.LocalPosition(card.transform, boardPosition, DEFAULT_TRANSITION_SPEED * 1.5f, 0.05f, Tween.EaseOut);
            Tween.LocalRotation(card.transform, Quaternion.Euler(10f, 0f, 0f), DEFAULT_TRANSITION_SPEED, 0.1f, Tween.EaseOut);
        }
        else
        {
            PlayerHand.instance.ParentCardToHand(card, (Vector3.up * 2f) + (Vector3.left * 2f));
            PlayerHand.instance.OnCardInspected(card);
        }
    }

    public void AssignCardToSlot(Card card, CardSlot slot, float speed = 1f)
    {
        if (card.Slot != null)
        {
            card.Slot.Card = null;
        }

        card.GetComponent<Collider>().enabled = false;
        slot.Card = card;
        card.Slot = slot;

        card.transform.parent = null;
        Tween.Position(card.transform, slot.transform.position + (Vector3.up * 0.025f), DEFAULT_TRANSITION_SPEED * speed, 0f, Tween.EaseInOut);
        Tween.Rotation(card.transform, slot.transform.GetChild(0).rotation, DEFAULT_TRANSITION_SPEED * speed, 0f, Tween.EaseInOut);
    }

    public void QueueCardForSlot(Card card, CardSlot slot, float speed = 1f, bool doTween = true)
    {
        card.GetComponent<Collider>().enabled = false;
        card.QueuedSlot = slot;

        if (doTween)
        {
            Tween.Position(card.transform, slot.transform.position + (Vector3.up * 0.025f) + (Vector3.forward * 2f),
                DEFAULT_TRANSITION_SPEED * speed, 0f, Tween.EaseInOut);
            Tween.Rotation(card.transform, slot.transform.GetChild(0).rotation, DEFAULT_TRANSITION_SPEED * speed, 0f, Tween.EaseInOut);
        }
    }

    public void OnSlotSelected(CardSlot slot)
    {
        if (currentValidSlots != null)
        {
            if (currentValidSlots.Contains(slot))
            {
                if (ChoosingSacrifices && slot.Card != null && !currentSacrifices.Contains(slot))
                {
                    slot.Card.Anim.SetMarkedForSacrifice(true);
                    currentSacrifices.Add(slot);
                }
                else
                {
                    LastSelectedSlot = slot;
                }
            }
        }
    }

    public List<CardSlot> GetAdjacentSlots(CardSlot slot)
    {
        var slotsToCheck = opponentSlots;
        if (playerSlots.Contains(slot))
        {
            slotsToCheck = PlayerSlots;
        }

        return slotsToCheck.FindAll(x => Mathf.Abs(slotsToCheck.IndexOf(x) - slotsToCheck.IndexOf(slot)) < 2 && x != slot);
    }

    public CardSlot GetAdjacent(CardSlot slot, bool adjacentOnLeft)
    {
        bool playerSlot = playerSlots.Contains(slot);
        var adjacent = GetAdjacentSlots(slot);
        if (adjacent.Count > 0)
        {
            if (playerSlot)
            {
                if (adjacentOnLeft)
                {
                    return adjacent.Find(x => SlotIsLeftOfSlot(x, slot, playerSlots));
                }
                else
                {
                    return adjacent.Find(x => !SlotIsLeftOfSlot(x, slot, playerSlots));
                }
            }
            else
            {
                if (adjacentOnLeft)
                {
                    return adjacent.Find(x => SlotIsLeftOfSlot(x, slot, opponentSlots));
                }
                else
                {
                    return adjacent.Find(x => !SlotIsLeftOfSlot(x, slot, opponentSlots));
                }
            }
        }
        return null;
    }

    public bool SlotIsLeftOfSlot(CardSlot slot1, CardSlot slot2, List<CardSlot> row)
    {
        return row.IndexOf(slot1) < row.IndexOf(slot2);
    }

    private bool SacrificesCreateRoomForCard()
    {
        foreach (CardSlot slot in PlayerSlots)
        {
            if (slot.Card == null)
            {
                return true;
            }
            if (CurrentSacrifices.Contains(slot) && slot.Card.Info.ability != SpecialAbility.Sacrificial)
            {
                return true;
            }
        }

        return false;
    }
}
