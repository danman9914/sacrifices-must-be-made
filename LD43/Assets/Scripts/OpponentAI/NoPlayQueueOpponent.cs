﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoPlayQueueOpponent : OpponentAI
{
    public override bool QueueFirstCardBeforePlayer { get { return false; } }

    public override IEnumerator DoPlayPhase()
    {
        QueueNewCards(doTween: false);
        PlayQueuedCards(playSpeed: 4f);
        yield break;
    }
}
