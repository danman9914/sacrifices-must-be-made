﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CursorType
{
    Default,
    Sacrifice,
    Place,
    Fight,
    Pickup,
    Flip,
}

public class Cursor3D : MonoBehaviour
{
    public static Cursor3D instance;

    public Interactable CurrentInteractable { get { return currentInteractable; } }
    private Interactable currentInteractable;

    public bool InteractionDisabled { get { return interactionDisabled; }
        set
        {
            if (!value)
            {
                currentInteractable.OnCursorExit();
                currentInteractable = null;
                SetCursorType(CursorType.Default);
            }

            interactionDisabled = value;
        }
    }
    private bool interactionDisabled;

    public Vector2 NormalizedPosition
    {
        get
        {
            float x = Mathf.Clamp(transform.localPosition.x / 8.5f, -1f, 1f);
            float y = Mathf.Clamp(transform.localPosition.y / 5f, -1f, 1f);
            return new Vector2(x, y);
        }
    }

    [SerializeField]
    private Camera rayCamera;

    [SerializeField]
    private List<Sprite> cursorTextures;

    [SerializeField]
    private List<Sprite> cursorDownTextures;

    private CursorType cursorType = CursorType.Default;

    void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        SetCursorType(CursorType.Default);
    }

    public void SetEnabled(bool enabled)
    {
        this.enabled = enabled;
    }

    private void FixedUpdate()
    {
        Vector2 mousePos = Input.mousePosition;

        Cursor.visible = false;

        Vector2 camSpace = Input.mousePosition / new Vector2(Screen.width, Screen.height);
        Vector2 localSpace = rayCamera.ViewportToWorldPoint(camSpace);
        float renderTextureMultiplier = 0.99f;
        transform.position = new Vector3(localSpace.x * renderTextureMultiplier, localSpace.y * renderTextureMultiplier, rayCamera.nearClipPlane);
    }

    void Update()
    {
        SetCursorDown(Input.GetButton("Select"));
        if (!InteractionDisabled)
        {
            SetCurrentInteractable();
            HandleClicks();
        }
    }

    private void SetCurrentInteractable()
    {
        Interactable hitInteractable = null;

        RaycastHit rayHit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out rayHit, 1000f))
        {
            hitInteractable = rayHit.transform.GetComponent<Interactable>();
        }

        if (hitInteractable != currentInteractable)
        {
            if (currentInteractable != null)
            {
                currentInteractable.OnCursorExit();
            }
            if (hitInteractable != null)
            {
                hitInteractable.OnCursorEnter();
            }
        }

        currentInteractable = hitInteractable;
    }

    public void SetCursorType(CursorType type)
    {
        cursorType = type;
    }

    private void SetCursorDown(bool down)
    {
        Vector2 hotSpot = new Vector2(60f, 60f);
        if (cursorType == CursorType.Sacrifice)
        {
            hotSpot = new Vector2(60f, 100f);
        }

        GetComponent<SpriteRenderer>().sprite = down ? cursorDownTextures[(int)cursorType] : cursorTextures[(int)cursorType];

    }

    private void HandleClicks()
    {
        if (currentInteractable != null)
        {
            if (Input.GetButtonDown("Select"))
            {
                currentInteractable.OnCursorSelectStart();
            }
            else if (Input.GetButtonUp("Select"))
            {
                currentInteractable.OnCursorSelectEnd();
            }
        }
    }
}
